import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

import useInterval from "./shared/useInterval";

// Components
import Users from "./components/users";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const API_SERVER = process.env.REACT_APP_API_SERVER;
console.log(API_SERVER);
function App() {
  let [users, setUsers] = useState(null);

  const orderUsers = (response) => {
    let byBool = response.sort((x, y) => {
      return x === y ? 0 : x ? -1 : 1;
    });
    return byBool.sort((a, b) => {
      return b.infractions - a.infractions;
    });
  };

  useEffect(() => {
    fetch(API_SERVER + "/users", { method: "GET" })
      .then((res) => res.json())
      .then((response) => {
        setUsers(orderUsers(response));
      })
      .catch((err) => console.error(err));
  }, []);
  console.log(users);

  useInterval(() => {
    fetch(API_SERVER + "/users", { method: "GET" })
      .then((res) => res.json())
      .then((response) => setUsers(orderUsers(response)))
      .catch((err) => console.error(err));
  }, 1000);

  if (users) {
    // let renderUsers = users.map((user) => {
    //   return (
    //     <div>
    //       <div>Host:{user.id}</div>
    //       <div>Online:{user.online ? "Yes" : "No"}</div>
    //       <div>Infractions:{user.infractions}</div>
    //     </div>
    //   );
    // });

    return (
      <div className="App">
        <ToastContainer />

        <h2 className="users-header">
          {" "}
          <code>PG IRC Monitor</code> Users ({users.length})
        </h2>
        <div className="dashboard-container">
          <Users users={users}></Users>
        </div>
      </div>
    );
  } else {
    return <div>loading</div>;
  }
}

export default App;
