import React, { useState, useEffect } from "react";
import moment from "moment";
import { toast } from "react-toastify";
import useInterval from "../../shared/useInterval";

import "./users.css";

const API_SERVER = process.env.REACT_APP_API_SERVER;

const Users = (props) => {
  const [userInFocus, setUserInFocus] = useState(null);
  const [messagesInFocus, setMessagesInFocus] = useState(null);
  const [messages, setMessages] = useState({ notSet: true, length: 99999 });

  const handleUserInFocus = (user) => {
    setUserInFocus(user);
    fetch(API_SERVER + "/user/" + user.id + "/messages", { method: "GET" })
      .then((res) => res.json())
      .then((response) => {
        setMessagesInFocus(response);
      })
      .catch((err) => console.error(err));
  };

  useInterval(() => {
    fetch(API_SERVER + "/messages", { method: "GET" })
      .then((res) => res.json())
      .then((response) => {
        if (messages.length < response.length) {
          toast.warn("New profanity detected");
        }
        let newRes = response.sort(function(a, b) {
          return new Date(b.timestamp) - new Date(a.timestamp);
        });

        setMessages(newRes);
      })
      .catch((err) => console.error(err));
  }, 1000);

  const getUserInfo = (user) => {
    return new Promise((resolve, reject) => {
      fetch(API_SERVER + "/user/" + user.id, {
        method: "GET"
      })
        .then((res) => res.json())
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  };
  return (
    <React.Fragment>
      <div className="users-container">
        {props.users && props.users.length > 0
          ? props.users.map((user) => {
              return (
                <div
                  className="user-item"
                  onClick={(e) => handleUserInFocus(user)}
                  style={
                    user.online
                      ? { backgroundColor: "rgb(56, 110, 68)" }
                      : { backgroundColor: "rgb(110, 56, 56)" }
                  }
                >
                  <div>
                    <b>Host:</b>
                    {"  "} {user.id}
                  </div>
                  <div>
                    <b>Online:</b>
                    {"  "} {user.online ? "Yes" : "No"}
                  </div>
                  <div>
                    <b>Infractions:</b>
                    {"  "} {user.infractions}
                  </div>
                </div>
              );
            })
          : ""}
      </div>
      <div className="right-panel">
        <div className="recent-infractions widget-style">
          <b>Most Recent Profane Messages ({messages.length})</b>
          <div className="recent-infractions-messages">
            {messages && !messages.notSet
              ? messages.map((message) => {
                  return (
                    <div>
                      <b>{moment(message.timestamp).format("lll")}</b> -{" "}
                      {message.user} - {message.message}
                    </div>
                  );
                })
              : "No profane messages logged"}
          </div>
        </div>
        <div className="user-infocus widget-style">
          {userInFocus ? (
            <div>
              <b>Selected User</b>
              <div>
                <b>User Hostname:</b> {userInFocus.id}
              </div>
              <div>
                <b>Profane Messages:</b>
                <br></br>
              </div>
              <div className="user-messages">
                {messagesInFocus
                  ? messagesInFocus.map((message) => {
                      return (
                        <div>
                          <b>{moment(message.timestamp).format("lll")}</b> -{" "}
                          {message.message}
                        </div>
                      );
                    })
                  : "No profane messages logged"}
              </div>
            </div>
          ) : (
            "Select a user"
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

export default Users;
