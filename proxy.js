const net = require("net");
const Filter = require("bad-words"),
  filter = new Filter();

const fetch = require("node-fetch");
process.on("uncaughtException", function(error) {
  console.error(error);
});

var localport = 6667;
var remotehost = "chat.scoutlink.net";
var remoteport = 6668;
const API_SERVER = "http://127.0.0.1:1337";

const updateUser = (userID, online) => {
  return new Promise((resolve, reject) => {
    fetch(API_SERVER + "/user/" + userID, {
      method: "POST",
      body: JSON.stringify({
        online: online
      }),
      headers: { "Content-Type": "application/json" }
    })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const insertMessage = (userID, message, profane) => {
  console.log("inserting message");
  return fetch(API_SERVER + "/message/" + userID, {
    method: "POST",
    body: JSON.stringify({
      message,
      profane
    }),
    headers: { "Content-Type": "application/json" }
  });
};

var server = net.createServer(function(localsocket) {
  var remotesocket = new net.Socket();

  remotesocket.connect(remoteport, remotehost);

  localsocket.on("connect", async function(data) {
    try {
      await updateUser(localsocket.remoteAddress, true);
      console.log("updated the user yo");
    } catch (err) {
      console.error(err);
    }
    console.log(
      ">>> connection #%d from %s:%d",
      server.connections,
      localsocket.remoteAddress,
      localsocket.remotePort
    );
  });

  localsocket.on("data", async function(data) {
    try {
      await updateUser(localsocket.remoteAddress, true);
      console.log("updated the user yo");
    } catch (err) {
      console.error(err);
    }
    console.log(
      "%s:%d - writing data to remote",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    let message = data.toString();
    if (message.includes("PRIVMSG") && filter.isProfane(message)) {
      console.log(`Profanity from ${localsocket.remoteAddress} - ${message}`);
      await insertMessage(
        localsocket.remoteAddress,
        message,
        filter.isProfane(message)
      );
    } else {
      var flushed = remotesocket.write(data);
      if (!flushed) {
        console.log("  remote not flushed; pausing local");
        localsocket.pause();
      }
    }
  });

  remotesocket.on("data", function(data) {
    console.log(
      "%s:%d - writing data to local",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    var flushed = localsocket.write(data);
    if (!flushed) {
      console.log("  local not flushed; pausing remote");
      remotesocket.pause();
    }
  });

  localsocket.on("drain", function() {
    console.log(
      "%s:%d - resuming remote",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    remotesocket.resume();
  });

  remotesocket.on("drain", function() {
    console.log(
      "%s:%d - resuming local",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    localsocket.resume();
  });

  localsocket.on("close", async function(had_error) {
    try {
      await updateUser(localsocket.remoteAddress, false);
      console.log("updated the user yo");
    } catch (err) {
      console.error(err);
    }
    console.log(
      "%s:%d - closing remote",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    remotesocket.end();
  });

  remotesocket.on("close", function(had_error) {
    console.log(
      "%s:%d - closing local",
      localsocket.remoteAddress,
      localsocket.remotePort
    );
    localsocket.end();
  });
});

server.listen(localport);

console.log(
  "redirecting connections from 127.0.0.1:%d to %s:%d",
  localport,
  remotehost,
  remoteport
);
