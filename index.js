const express = require("express");
const bodyParser = require("body-parser");
const low = require("lowdb");
const FileAsync = require("lowdb/adapters/FileAsync");

const user = require("./controllers/user");
const message = require("./controllers/message");

// Init DB

// API Server

const app = express();
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

const PORT = 1337;

// Create database instance and start server
const adapter = new FileAsync("db.json");
low(adapter)
  .then((db) => {
    db._.mixin({
      upsert: function(collection, obj, key) {
        key = key || "id";
        for (var i = 0; i < collection.length; i++) {
          var el = collection[i];
          if (el[key] === obj[key]) {
            collection[i] = obj;
            return collection;
          }
        }
        collection.push(obj);
      }
    });
    // Get Routes
    app.get("/users", (req, res) => {
      console.info("getting users...");
      user
        .getUsers(db)
        .then((docs) => {
          docs.forEach(async (doc) => {
            let infractionNo = await message.getUserInfractionsCount(
              db,
              doc.id
            );
            console.log(infractionNo);
            doc.infractions = infractionNo.infractions;
          });
          res.status(200).json(docs);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    app.get("/user/:userID", (req, res) => {
      user
        .getUser(db, req.params.userID)
        .then((doc) => {
          message
            .getUserInfractionsCount(db, req.params.userID)
            .then((infractions) => {
              res.status(200).json({ ...doc[0], ...infractions });
            })
            .catch((error) => {
              console.error(error);
              res.status(500).json({ error });
            });
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    app.get("/user/:userID/messages", (req, res) => {
      message
        .getUserMessages(db, req.params.userID)
        .then((doc) => {
          res.status(200).json(doc);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    app.get("/user/:userID/infractions", (req, res) => {
      message
        .getUserInfractionsCount(db, req.params.userID)
        .then((doc) => {
          res.status(200).json(doc);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    app.get("/messages", (req, res) => {
      message
        .getAllProfane(db)
        .then((doc) => {
          res.status(200).json(doc);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    // Post Routes
    app.post("/user/:userID", (req, res) => {
      console.log("user update request");
      console.info(req.body);
      user
        .insertOrUpdate(db, req.params.userID, req.body.online, false)
        .then((doc) => {
          res.status(200).json(doc);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    app.post("/message/:userID", (req, res) => {
      message
        .insert(db, req.params.userID, req.body.message, req.body.profane)
        .then((doc) => {
          res.status(200).json(doc);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ error });
        });
    });

    return db.defaults({ users: [], messages: [] }).write();
  })
  .then(() => {
    app.listen(PORT, () => console.log("listening on port " + PORT));
  });
