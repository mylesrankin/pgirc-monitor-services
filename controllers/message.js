// const db = require("../shared/db").db;
const shortid = require("shortid");
const moment = require("moment");

const insert = (db, hostname, message, profane) => {
  return new Promise((resolve, reject) => {
    try {
      db.get("messages")
        .upsert({
          id: shortid.generate(),
          user: hostname,
          timestamp: moment(),
          message,
          profane
        })
        .write();
      resolve();
    } catch (error) {
      reject(error);
    }
  });
};

const getUserMessages = (db, hostname) => {
  return new Promise((resolve, reject) => {
    try {
      let messages = db
        .get("messages")
        .filter({ user: hostname })
        .value();
      resolve(messages);
    } catch (error) {
      reject(error);
    }
  });
};

const getUserInfractionsCount = (db, hostname) => {
  return new Promise((resolve, reject) => {
    try {
      let messages = db
        .get("messages")
        .filter({ user: hostname, profane: true })
        .value();
      resolve({ infractions: messages.length });
    } catch (error) {
      reject(error);
    }
  });
};

const getAllProfane = (db) => {
  return new Promise((resolve, reject) => {
    try {
      let messages = db
        .get("messages")
        .filter({ profane: true })
        .value();
      resolve(messages);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  insert,
  getUserMessages,
  getUserInfractionsCount,
  getAllProfane
};
