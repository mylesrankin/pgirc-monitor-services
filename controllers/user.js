// const db = require("../shared/db").db;

const insertOrUpdate = (db, hostname, online, disable) => {
  return new Promise((resolve, reject) => {
    try {
      let result = db
        .get("users")
        .upsert({ id: hostname, online, disable })
        .write();
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};

const getUsers = (db) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log("getting users");
      let users = await db.get("users").value();
      resolve(users);
    } catch (error) {
      reject(error);
    }
  });
};

const getUser = (db, hostname) => {
  return new Promise((resolve, reject) => {
    try {
      let users = db
        .get("users")
        .filter({ id: hostname })
        .value();
      resolve(users);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = { insertOrUpdate, getUsers, getUser };
